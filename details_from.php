<script>
 function go_check() {
        if ($("#expires1").prop("checked")) {
            $('#hid_exp').css('display', 'none');
            $('#hid_start').css('display', 'none');
			$("#coupon_expiry_date").prop('required',false);
			$("#coupon_start_date").prop('required',false);
        } else {
            $('#hid_exp').css('display', 'block');
            $('#hid_start').css('display', 'block');
            $("#coupon_start_date").focus();
            $("#coupon_expiry_date").prop('required',true);
			$("#coupon_start_date").prop('required',true);
        }
    }
	$('#qty_quantity').on('click', function () {
        $('#insert_qty').removeClass('disable');
        $('#insert_qty').val('');
		$("#insert_qty").prop('required',true);
    });
    $('#unlimited_quantity').on('click', function () {
         $('#insert_qty').addClass('disable');
			$('#insert_qty').val('');
			$("#insert_qty").prop('required',false);
    });
	
	$('#coupon2').click(function(){	
		$("#cuponsuggestedtemplate").prop('required',true);
	});
	$('#coupon1').click(function(){	
		$("#cuponsuggestedtemplate").prop('required',false);
	});
	$('#premium').click(function(){	
		$("#price").prop('required',true);
	});
	$('#free').click(function(){	
		$("#price").prop('required',false);
	});
	$('#Reward').click(function(){	
		$("#price").prop('required',false);
	});
	$('#code_type_none').click(function(){	
		$('#code_type_barcode_div').hide();
		$('#code_type_coupon_code_div').hide();
		$('#coupon_code').prop('required',false);
		$('#barcode').prop('required',false);
		$('#coupon_code').val('');
		$('#barcode').val('');
		$('#barcode_image').val('');
		
	});
	$('#code_type_barcode').click(function(){	
		$('#code_type_barcode_div').show();
		$('#code_type_coupon_code_div').hide();
		$('#barcode').prop('required',true);
		$('#coupon_code').prop('required',false);
		$('#barcode').val('');
		$('#coupon_code').val('');
		$('#barcode_image').val('');
	});
	$('#code_type_coupon_code').click(function(){	
		$('#code_type_barcode_div').hide();
		$('#code_type_coupon_code_div').show();
		$('#coupon_code').prop('required',true);
		$('#barcode').prop('required',false);
		$('#coupon_code').val('');
		$('#barcode').val('');
		$('#barcode_image').val('');
	});
	$('#online_flagp').click(function(){
		$('.push_notification').show();
		$('#push_notification').val('1');
		$('#online_url').val('');
		$('.code_type_barcode').show();
		$('.online_url').hide();
		$('#online_url').prop('required',false);
	});
	$('#online_flag').click(function(){
		$('.push_notification').hide();
		$('#push_notification').val('');
		$('#online_url').val('');
		$('.code_type_barcode').hide();
		$('.online_url').show();
		$('#online_url').prop('required',true);
	});
$(document).ready(function(){		
	$('#barcode').change(function(){
	var barcode = $(this).val();
		$.ajax( {
		  type: 'POST',
		  dataType: "json",
		  url: '<?php echo base_url('ajax/barcode');?>',
		  data: {barcode:barcode},
		  success: function(data) {
		   $('#barcode-generated').html(data.barcode_img);
		   $('#barcode_image').val(data.barcodes);
		  }
		});
	});
	$('.datepicker').datepicker();
});           
</script>           
<?php 	//echo "<pre>"; print_r($owner_coupon_array); echo "</pre>"; ?>	   
<div class="modal-body">
			<div class="form-group">
			<label>Is Online Coupon? <span class="red-txt">*</span></label>
				<div class="row">
					<div class="col-sm-4">
						<input type="radio" id="online_flag" name="online_flag" value="O" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['online_flag'] == 'O'){ echo 'checked="checked"'; } ?>>
						<label for="online_flag">Yes</label>
					</div>
					<div class="col-sm-5">
						<input type="radio" id="online_flagp" name="online_flag" value="P" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['online_flag'] == 'P'){ echo 'checked="checked"'; } ?>>
						<label for="online_flagp">No</label>
					</div>		
				</div>
			</div>
			<?php if($buss_id){ ?>
			<div class="form-group">
			<label>Choose Coupon Type <span class="red-txt">*</span></label>
				<div class="row">
					<div class="col-sm-4">
					<input type="radio" id="free" name="premium_type" value="F" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['premium_type'] == 'F'){ echo 'checked="checked"'; } ?>>
					<label for="free">Free Coupon</label>
					</div>
					<div class="col-sm-5">
					<input type="radio" id="premium" name="premium_type" value="P" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['premium_type'] == 'P'){ echo 'checked="checked"'; } ?>>
					<label for="premium">Premium Coupon</label>
					</div>				
					<div class="col-sm-3">
						<input type="radio" id="Reward" name="premium_type" value="R" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['premium_type'] == 'R'){ echo 'checked="checked"'; } ?>>
						<label for="Reward">Reward Coupon</label>
					</div>			
				</div>
			</div>
			<?php } ?>
            		
			<input type="hidden" name="coupon_id" id="coupon_id" value="<?php echo isset($coup_id) ? $coup_id :''; ?>" />  
                    <div class="form-group">
					<label>Choose Coupon Category<span class="red-txt">*</span></label>
                        <div class="row">
                            <div class="col-sm-4 pad-rt-0 new-Couponrad">
                                <div class="radio-group mar-top-30 ">
                                    <div class="pull-left mar-rt-15">
                                        <input type="radio" id="coupon1" name="new_coupon" value="N" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['new_coupon'] == 'N'){ echo 'checked="checked"'; } ?>>
                                        <label for="coupon1">New Coupon</label>
                                    </div>
                                    <div class="pull-right">
                                        <input type="radio" id="coupon2" name="new_coupon" value="X" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['new_coupon'] == 'X'){ echo 'checked="checked"'; } ?>>
                                        <label for="coupon2" class="whit-txt">.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <label> Choose from suggested coupons</label>									
								<?php
								$cupon['']='Select';
								$sel_cpn = (!empty($owner_coupon_array) && $owner_coupon_array[0]['suggest_coupon_id'])? $owner_coupon_array[0]['suggest_coupon_id']:'';
								if(!empty($suggest_coupon)){
									foreach($suggest_coupon as $key=> $val){ 					
										$cupon[$val['coupon_id']] = $val['name']; 
									}
								}
								echo form_dropdown('suggest_coupon_id', $cupon, $sel_cpn, 'class="form-control cupontemplate"  id="cupontemplate" ');
								?>
                            </div>
                        </div>
                    </div>
					<?php if($ownerlist){ ?>
					<div class="form-group">
						<label>Choose Owner</label>
						<div class="inp-fild">
						<?php
						   $owner['']='Select';
							$sel_state = (!empty($owner_coupon_array) && $owner_coupon_array[0]['owner_id'])? $owner_coupon_array[0]['owner_id']:'';
							if(!empty($ownerlist)){
							 foreach($ownerlist as $key=> $vals){ 					
								$owner[$vals['id']] = $vals['owner_contact_person']; 
							 }
							}
							echo form_dropdown('owner_id', $owner, $sel_state, 'class="form-control" required="required" id="owner_contact_person" onchange="get_location_mode(this.value)"');
						   ?>
						<?php echo form_error('owner_contact_person', '<div class="error_msg">', '</div>'); ?> </div>
						<div class="sm-txt" id="locDiv"></div>
					</div>
					<?php }

					?>
                    <div class="form-group">
                        <label>Coupon title <span class="red-txt">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Coupon title" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['name'])?$owner_coupon_array[0]['name']:'';?>" required="">
						<?php echo form_error('name', '<div class="error">', '</div>'); ?> 
                    </div>
                    <div class="form-group">
                        <label>Description <span class="red-txt">*</span></label>
                        <textarea class="form-control" name="details" id="details" cols="" rows="" required><?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['details']!='')?$owner_coupon_array[0]['details']:'';?></textarea>
						<?php echo form_error('details', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="form-group">
                        <label>Disclaimer <span class="red-txt">*</span></label>
                        <textarea class="form-control" name="disclaimer" id="disclaimer" cols="" rows="" required><?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['disclaimer']!='')?$owner_coupon_array[0]['disclaimer']:'Not valid with any other discounts or coupons';?></textarea>
						<?php echo form_error('disclaimer', '<div class="error">', '</div>'); ?>
                        <span class="gray-txt">Example:Not valid with any other discounts or coupons.</span>
                </div>                    
                    <div class="form-group">
                        <label>Expires </label>
                        <div class="radio-group-wrap">
                            <div>
                                <input type="radio" id="expires1" name="coupon_expiry_mode" onclick="go_check()" value="F" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_expiry_mode'] == 'F'){ echo 'checked="checked"'; } ?>> 
                                <label for="expires1">When deactivated</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 pull-left">
                                    <input type="radio" id="expires2" name="coupon_expiry_mode" onclick="go_check()" value="C" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_expiry_mode'] == 'C'){ echo 'checked="checked"'; } ?>>
                                    <label for="expires2">Custom</label>
                                </div>
								
                                <div class="col-sm-3"> 
                                    <div class="form-group">
                                    <div class='date-datepicker datepicker-modal-position ' id="hid_start">
                                    <input type='text' class="form-control datepicker <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_expiry_mode'] == 'F'){ echo "hide"; }?>" id='coupon_start_date' name="coupon_start_date" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_start_date']!='0000-00-00' && $owner_coupon_array[0]['coupon_start_date']!=NULL)?date('m/d/Y',strtotime($owner_coupon_array[0]['coupon_start_date'])):'';?>"/> 
                                    </div>
                                </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class='date-datepicker datepicker-modal-position ' id="hid_exp"> 
									<input type='text' id='coupon_expiry_date' class="form-control datepicker <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_expiry_mode'] == 'F'){ echo "hide"; }?>" id='coupon_expiry_date' name="coupon_expiry_date" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_expiry_date']!='0000-00-00' && $owner_coupon_array[0]['coupon_expiry_date']!=NULL)?date('m/d/Y',strtotime($owner_coupon_array[0]['coupon_expiry_date'])):'';?>"/> 
									</div>
									</div>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <label>Premium Value</label>
                        <div class="row">
                            <div class="col-sm-4">
							 <input class="form-control" type="text" name="price" id="price" placeholder="Enter Premium Value" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['price'])?$owner_coupon_array[0]['price']:'';?>" maxlength="12" />
							</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <div class="row">
							<div class="radio-group col-sm-4">
								<input type="radio" id="qty_quantity" name="quantity_number" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['quantity'] >0){ echo 'checked="checked"'; } ?>  value="1">
								<label for="qty_quantity" class="mar-top-10" style="float: left;"><span></span></label>
								<?php
								$class_qty = 'disable'; 				
								if(!empty($owner_coupon_array) && $owner_coupon_array[0]['quantity'] >0){ 
								$class_qty = ''; 
								} ?>
								<input type="text" style="width: 110px; margin-bottom: 5px;" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['quantity'])?$owner_coupon_array[0]['quantity']:'';?>" placeholder="Enter Quantity" name="quantity" id="insert_qty" class="form-control <?php echo $class_qty; ?>"> 					
							</div>

							<div class="radio-group col-sm-4 mar-top-10 ">
								<input type="radio" id="unlimited_quantity" name="quantity_number" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['quantity'] ==0){ echo 'checked="checked"'; } ?> value="0">
								<label for="unlimited_quantity"><span>Unlimited</span></label>
							</div>
				
                        </div>
                    </div>
					  <div class="form-group">
                        <label>Code Type</label>
                        <div class="row">
							<div class="radio-group col-sm-4 mar-top-10 ">
								<input type="radio" id="code_type_none" name="coupon_type" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_type'] ==0){ echo 'checked="checked"'; } ?> value="0">
								<label for="code_type_none"><span>None</span></label>
							</div>
							<div class="radio-group col-sm-4 mar-top-10 code_type_barcode" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['online_flag'] == 'O'){ ?> style="display: none;" <?php } ?>>
								<input  type="radio" id="code_type_barcode" name="coupon_type" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_type'] ==1){ echo 'checked="checked"'; } ?> value="1" >
								<label for="code_type_barcode"><span>Bar code</span></label>
							</div>

							<div class="radio-group col-sm-4 mar-top-10 ">
								<input type="radio" id="code_type_coupon_code" name="coupon_type" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_type'] ==2){ echo 'checked="checked"'; } ?> value="2">
								<label for="code_type_coupon_code"><span>Coupon Code</span></label>
							</div>
				
                        </div>
                    </div>
					
                    <div id="code_type_barcode_div" class="form-group" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_type'] != 1){ ?> style="display: none;" <?php } ?>>
                        <label>Bar code</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Enter a 12 digit code" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['barcode'])?$owner_coupon_array[0]['barcode']:'';?>">
								</div>
									<?php 
									$barimage= 'Bar-code.jpg';									
									if(!empty($owner_coupon_array) && $owner_coupon_array[0]['barcode_image']){
										$barimage= $owner_coupon_array[0]['barcode_image'];
									}
									?>
                            <div class="col-sm-4" id="barcode-generated">
							<img src="<?php echo base_url('assets/barcode/'.$barimage)?>" alt=""/>
							</div>
                        </div>
						<input type="hidden" class="form-control" name="barcode_image" id="barcode_image" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['barcode_image'])?$owner_coupon_array[0]['barcode_image']:'';?>">
                    </div>
                    <div id="code_type_coupon_code_div" class="form-group " <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_type'] != 2){ ?> style="display: none;" <?php } ?>>
                        <label>Coupon code</label>
                        <div class="row">
                            <div class="col-sm-4">
							 <input class="form-control" type="text" name="coupon_code" id="coupon_code" placeholder="Enter Coupon Code" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['coupon_code'])?$owner_coupon_array[0]['coupon_code']:'';?>" maxlength="12" />
							</div>
                        </div>
                    </div>
					<div class="form-group online_url" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['online_flag'] == 'P'){ ?> style="display: none;" <?php } ?>>
                        <label>Web Url<span class="red-txt">*</span></label>
                        <input type="text" class="form-control" name="online_url" id="online_url" placeholder="Online Url" value="<?php echo (!empty($owner_coupon_array) && $owner_coupon_array[0]['online_url'])?$owner_coupon_array[0]['online_url']:'';?>">
                    </div>
					<?php
					$upgradeclass="disable";				
					if(isset($owner_coupon_array) && !empty($owner_coupon_array) && $owner_coupon_array[0]['is_buy']==1){
						$upgradeclass="";
					}
					?>
                    <div class="form-group push_notification" <?php if(!empty($owner_coupon_array) && $owner_coupon_array[0]['online_flag'] == 'O'){ ?> style="display: none;" <?php } ?>>
                        <label>Push Notifications (Premium Feature)</label>
                        <div class="dis-switch <?php echo $upgradeclass;?>"> 
						Off <label class="switch">						
						<input value="1" type="checkbox" name="push_notification" class="onoffswitch-checkbox" id="push_notification" <?php if(isset($owner_coupon_array[0]['push_notification']) && $owner_coupon_array[0]['push_notification']==1) {echo 'checked';}?>>						
                          <span class="slider round"></span>
                        </label> On </div>
                    </div></div>
						<div class="modal-footer">
							<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'btn btn-success','style'=>'','value'=>(isset($coup_id))?'Update coupon':'Create coupon'));?>
							<button type="button" class="btn btn-close view-popup-back" data-dismiss="modal">Back</button>
						</div>
            </div>
  