
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="row">
        <div class="ico col-sm-2">
            <div>
                <span>
                    <?php
				$image="demo-logo.png";
				if($feature->image!='' && file_exists(FCPATH.'assets/features_logo/'.$feature->image)){
				$image=$feature->image;
				}
				?>
                    <img src="<?php echo base_url('assets/features_logo/'.$image)?>" alt="" />
                </span>
            </div>
        </div>
        <div class="ico col-sm-10">
            <h4><?php echo $feature->title;?></h4>
            <p class="gray-txt"><?php //echo $feature->description;?></p>
        </div>
    </div>
</div>
<div class="modal-section modal-body">
    <div class="pad-10">
    <p>Choose Location</p>
    <table id="tblFruits" style="width:100%;">
        <?php 
if(!empty($query_loc_array)){
foreach($query_loc_array as $key=> $location){
	$feature_data=array();
	if(!empty($location) && $location['feature_ids']!=''){
		if(is_serialized_string($location['feature_ids'])){
			$feature_data = unserialize($location['feature_ids']);
		}
	}
	$featurer_select ='';
	if(!empty($feature_data)){
		foreach($feature_data as $key => $ids){
			if($feature->id==$key){
				$featurer_select ="Checked";
				$featurer_class ="feature_disable";
			}
		}
		$select_feature = $feature_data[$feature->id];
		$exp_times= array_keys($select_feature);
		$current_time = strtotime(date('Y-m-d'));
		if(max($exp_times) <= $current_time){
				$featurer_select= "";
				$featurer_class= "";
		}
	}
	?>

        <tr>
            <td style="padding:10px; border-bottom:1px solid #f2f6fa">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="location[]" class="custom-control-input locationn_feature <?php echo $featurer_class;?>" id="location<?php echo $key; ?>" value="<?php echo $location['id'];?>" <?php echo $featurer_select;?> >
                    <label class="custom-control-label" for="location<?php echo $key; ?>">
                        <h4 style="margin-top:-5px;margin-bottom: 0;"><?php echo $location['address1'];?></h4>
                    </label>
                </div>
                <p class="gray-txt"><?php echo $location['city'];?>, <?php echo $location['zip'];?> <span>•</span> <?php echo $location['phone'];?> <span>•</span> <?php echo $location['email'];?></p>
            </td>
        </tr>

        <?php 
}
}
?>
    </table>
    <div style="overflow: hidden;margin:15px 0;">
        <input type="hidden" value="<?php echo $feature->id;?>" id="featured_id">
        <!--<input type="button" class="btn-vil pull-right" id="btnGet" value="Get" />-->
    </div>
    <section class="">
        <div class="row">
            <div class="ico col-sm-9">
                <p class="gray-txt"><?php //echo $feature->feature_plan;?></p>
            </div>
            <div class="col-sm-3">
                <div>
                    <span>
                        <?php
						$preview_image="demo-logo.png";
						if($feature->preview_image!='' && file_exists(FCPATH.'assets/features_logo/preview/'.$feature->preview_image)){
						$preview_image=$feature->preview_image;
						}
						?>
                        <img src="<?php echo base_url('assets/features_logo/preview/'.$preview_image)?>" alt="" />
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            
                <!------------------------------------------------------------------------------------->
                <?php 
							$part_monthly = explode('.', $feature->price_monthly);
							$part_yearly = explode('.', $feature->price_yearly);
							$monthly_amt = '$'.number_format($part_monthly[0]).'.'.$part_monthly[1];
							$yearly_amt = '$'.number_format($part_yearly[0]).'.'.$part_yearly[1];
							?>
                <div class="col-sm-12">
                    <div class="pull-left">
                    <h3 class="text-right"><?php echo $monthly_amt?></h3>
                    <p class="text-right">month</p>
                        </div>
                    <div class="radio-group pull-right mar-top-15">
                        <div class="pull-left mar-rt-15">
                            <input type="radio" id="ppm11_<?php echo $feature->id;?>" name="choose_package_time" value="m" class="choose_package_time" data-billed_amount="<?php echo "Billed ".$monthly_amt." monthly";?>" data-id="<?php echo $feature->id;?>"checked>
                            <label for="ppm11_<?php echo $feature->id;?>">Price per month</label>
                        </div>
                        <div class="pull-left">
                            <input type="radio" id="ppy11_<?php echo $feature->id;?>" name="choose_package_time" value="y" class="choose_package_time" data-billed_amount="<?php echo "Billed ".$yearly_amt." annualy";?>" data-id="<?php echo $feature->id;?>">
                            <label for="ppy11_<?php echo $feature->id;?>">Price per year</label>
                        </div>
                    </div>

                </div>
                <!------------------------------------------------------------>
            
        </div>
    </section>
</div></div>
<div class="popup-footer">
    <a class="ajax-call pull-left btn-success <?php echo $buyclass;?> feature_buy hide" href="<?php echo base_url('admin/owners/features/save/'.$feature->id.'/'.$owner_id)?>" data-preiod="" data-featured="" id="buys_<?php echo $feature->id;?>">Buy</a>
    <div class="pull-right">
        <div class="text-right font-20">
            <div class="gray-txt" id="billed_amount_<?php echo $feature->id;?>">Billed <?php echo $monthly_amt;?> annualy</div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    $(document).on('change', '.choose_package_time', function() {
        var billed_amt = $(this).data("billed_amount");
        var billed_id = $(this).data("id");
        $('#billed_amount_' + billed_id).text(billed_amt);
        $('#buys_' + billed_id).data('preiod', $(this).val());
    });

    $(function() {
        $(".locationn_feature").click(function() {
            var billed_id = $('#featured_id').val();
            //Create an Array.
            var selected = new Array();

            //Reference the CheckBoxes and insert the checked CheckBox value in Array.
            $("#tblFruits input[type=checkbox]:checked").each(function() {
                selected.push(this.value);
            });

            //Display the selected CheckBox values.
            if (selected.length > 0) {
                //alert("Selected values: " + selected.join(","));
                $('#buys_' + billed_id).data('featured', selected.join(","));
				$('.feature_buy').removeClass('hide');
            }else{
				$('.feature_buy').addClass('hide')
			}
        });
    });

</script>
