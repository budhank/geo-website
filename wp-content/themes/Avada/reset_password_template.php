<?php
/**
 * Template Name: Reset Password template
 *
 */
get_header(); 
?>
<script type="text/javascript">
jQuery(document).ready(function(e) {
	jQuery('#reset').click(function(e) {
		var err = 0;
		var new_password = jQuery('#new_password').val();
		var repeat_password = jQuery('#repeat_password').val();
		if(new_password == ''){
			err = 1;
			alert('Please enter new password.');
			return false;
		}
		if(repeat_password == ''){
			err = 1;
			alert('Please enter repeat password.');
			return false;
		}
		if(new_password!='' && repeat_password!='' && new_password!=repeat_password){
			err = 1;
			alert('New password and repeat password should be same.');
			return false;
		}
		
		if(err == 0){
			return true;
		}
		
		
	});
});
</script>
<?php
$mydb = new wpdb('geocoupo_john','[=l07.G~i{#J','geocoupo_system_v2','localhost');
$rows = $mydb->get_results("SELECT * FROM mob_signup WHERE reset_pass_link_id='".$_REQUEST['reset_id']."' AND reset_password_status=0");
if(count($rows)>0){
?>
<center>
<form name="reset_frm" action="" method="post" id="reset_frm">
<input type="hidden" name="hid_reset" value="1">

<h3>RESET YOUR PASSWORD</h3>
<br />
New password: (required)
<br />
<input type="password" name="new_password" id="new_password" />
<br />
Repeat password: (required)
<br />
<input type="password" name="repeat_password" id="repeat_password" />
<br />
<input type="submit" name="submit" value="Reset" id="reset" />
</form>
</center>
<?php
} else {
?>
<center>
<h3>Invalid/Expired password reset Link</h3>
</center>
<?php
}

if(isset($_REQUEST['hid_reset']) && $_REQUEST['hid_reset']==1 && count($rows)>0){
	
	 $table = "mob_signup";
     $data_array = array('password' => md5($_REQUEST['new_password']), 'reset_password_status' => 1);
     $where = array('reset_pass_link_id' => $_REQUEST['reset_id']);
	
	 $mydb->update( $table, $data_array, $where );
	 echo '<center><h4>Password Reset Successfully.</h4><center>';
	
}
?>
<?php get_footer(); ?>

