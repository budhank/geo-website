<?php
/**
 * Template Name: Location Listing template
 *
 */
get_header(); 
?>
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script type='text/javascript' src='http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/gmaps.js'></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory'); ?>/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory'); ?>/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

<?php

if(isset($_REQUEST['mid']) && !empty($_REQUEST['mid'])){
	
	$mid = base64_decode($_REQUEST['mid']);
	$mydb = new wpdb('geocoupo_john','[=l07.G~i{#J','geocoupo_system_v2','localhost');
	$i = 0 ;
	$rows = $mydb->get_results("SELECT mob_mst_business.bcat_id,mob_mst_business.name,mob_business_location.latitude,mob_business_location.lognitude,
mob_business_location.address1,mob_business_location.address2,mob_business_location.city,mob_business_location.state_initial,mob_mst_business.logo_img,mob_business_location.zip FROM mob_mst_business 
INNER JOIN `mob_business_location` ON `mob_mst_business`.`buss_id` = `mob_business_location`.`buss_id`
INNER JOIN `mob_mst_bcategory` ON `mob_mst_bcategory`.`cat_id` = `mob_mst_business`.`bcat_id` 
WHERE `mob_mst_business`.`buss_id`=".$mid." ORDER BY mob_business_location.address1"); 
	if(count($rows)>0){
		$totlocation = count($rows);
		if($totlocation>0){
		$locationStr = 	'&nbsp;-&nbsp;'.$totlocation.'&nbsp;Locations';
		}
		else{
		$locationStr = 	'&nbsp;-&nbsp;'.$totlocation.'&nbsp;Location';	
		}
	?>
	<div class="mp_ar_nm" style="border-bottom:1px solid #CCC; margin-bottom:15px; overflow:hidden;">
		<div style="float:left;">
        <img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/coupon-app-v2/assets/business-logo/<?php echo $rows[0]->logo_img;?>" alt="" style="height:100px;">
        </div>
        <div style="float:left;">
        <h3 class="title2" style="border:none; margin-top: 40px;padding-left: 15px;">
		<?php echo $rows[0]->name.$locationStr; ?>
        </h3>
	    </div>
    </div>
  <?php /* === THIS IS WHERE WE WILL ADD OUR MAP USING JS ==== */ ?>
				<div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
					<div id="google-map" class="google-map">
					</div><!-- #google-map -->
				</div>
				<?php /* === MAP DATA === */ ?>
				<?php
				$locations = array();
				
				
				foreach($rows as $obj){
					$location_address = '';
					
					if($obj->address1 !=''){
						$location_address .= $obj->address1.',&nbsp;';
					}
					if($obj->address2 !=''){
						$location_address .= $obj->address2.',&nbsp;';
					}
					if($obj->city !=''){
						$location_address .= $obj->city.',&nbsp;';
					}
					if($obj->state_initial !=''){
						$location_address .= $obj->state_initial.',&nbsp;';
					}
					if($obj->zip !=''){
						$location_address .= $obj->zip;
					}
					/* Marker #1 */
					$locations[] = array(
						'google_map' => array(
							'lat' => $obj->latitude,
							'lng' => $obj->lognitude,
						),
						'location_name'    => mysql_real_escape_string(htmlspecialchars($obj->name)),
						'location_address' => $location_address,
						'cat_id' 		   => $obj->bcat_id
					);
				}
				?>
				<?php /* === PRINT THE JAVASCRIPT === */ ?>
				<?php
				/* Set Default Map Area Using First Location */
				$map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : '';
				$map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : '';
				?>
				<script>
				jQuery( document ).ready( function($) {
					/* Do not drag on mobile. */
					var is_touch_device = 'ontouchstart' in document.documentElement;
					var map = new GMaps({
						el: '#google-map',
						lat: '<?php echo $map_area_lat; ?>',
						lng: '<?php echo $map_area_lng; ?>',
						scrollwheel: false,
						draggable: ! is_touch_device
					});
					/* Map Bound */
					var bounds = [];
					<?php /* For Each Location Create a Marker. */
					foreach( $locations as $location ){
						
						
						$name = $location['location_name'];
						$map_lat = $location['google_map']['lat'];
						$map_lng = $location['google_map']['lng'];
						$address = $location['location_address'];
						$cat_id = $location['cat_id'];
						?>
						/* Set Bound Marker */
						var latlng = new google.maps.LatLng(<?php echo $map_lat; ?>, <?php echo $map_lng; ?>);
						bounds.push(latlng);
						/* Add Marker */
						map.addMarker({
							lat: <?php echo $map_lat; ?>,
							lng: <?php echo $map_lng; ?>,
							<?php if($cat_id==2){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_2.png"},
							<?php }	?>
							<?php if($cat_id==3){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_3.png"},
							<?php }	?>
							<?php if($cat_id==4){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_4.png"},
							<?php }	?>
							<?php if($cat_id==5){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_5.png"},
							<?php }	?>
							<?php if($cat_id==6){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_7.png"},
							<?php }	?>
							<?php if($cat_id==7){?>
							icon: {url:"http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_1.png"},
							<?php }	?>
							title: '<?php echo stripslashes(mysql_real_escape_string($name)); ?>',
							
							infoWindow: {
								content: '<p><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/coupon-app-v2/assets/business-logo/<?php echo $rows[0]->logo_img;?>" alt="" style="height:75px;"></p><p><h3><?php echo stripslashes(mysql_real_escape_string($name)); ?></h3></p><p><?php echo stripslashes(mysql_real_escape_string($address)); ?></p>'
							}
						});
					<?php } //end foreach locations ?>
					/* Fit All Marker to map */
					map.fitLatLngBounds(bounds);
					/* Make Map Responsive */
					var $window = $(window);
					function mapWidth() {
						var size = $('.google-map-wrap').width();
						$('.google-map').css({width: size + 'px', height: (size/2) + 'px'});
					}
					mapWidth();
					$(window).resize(mapWidth);
				});
				</script>
                <section class="viw_loc">
        			<div class="avada-row">
                        <div>
                            <table style="margin-bottom:15px; margin-top:-45px; font-size:16px;" width="100%">
                            <tr>
                            <td width="15%">
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_2.png" alt=""></span>
                                <span style="margin-top:15px;"> => Automotive</span>
                            </td>
                            <td>
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_3.png" alt=""></span> 
                                <span> => Entertainment</span>
                            </td>
                            <td>
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_4.png" alt=""></span>
                                <span> => Health & Beauty</span>
                            </td>
                            <td>
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_5.png" alt=""></span>
                                <span> => Petcare</span>
                            </td>
                            <td>
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_7.png" alt=""></span>
                                <span> => Restaurants</span>
                            </td>
                            <td>
                                <span><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-content/themes/Avada/place_1.png" alt=""></span>
                                <span> => Shopping & Services</span>
                            </td>
                            </tr>
                            </table>
                		</div>
                        <div class="viw_loc_tit">
                            <h3 class="title2">Locations List - <?php echo count($rows); ?></h3>
                        </div>
                        <div class="viw_loc_data location_list_wrap">
                            <table class="table">
                                <tbody>
                                 <?php
								 $i=1;
								 foreach ($rows as $obj){
									 //echo "SELECT count(*) as total_coupon FROM mob_business_coupon WHERE buss_id=".$mid;
									$totrowsres = $mydb->get_results("SELECT count(*) as total_coupon FROM mob_business_coupon WHERE buss_id=".$mid."");
									$totrows = $totrowsres[0]->total_coupon > 0?$totrowsres[0]->total_coupon:'';
									$location_address = '';
									$loc_str = '';
									if($obj->address1 !=''){
										$location_address .= $obj->address1.',&nbsp;';
										$loc_str .= $obj->address1.'|';
									}
									if($obj->address2 !=''){
										$location_address .= $obj->address2.',&nbsp;';
										$loc_str .= $obj->address2.'|';
									}
									if($obj->city !=''){
										$location_address .= $obj->city.',&nbsp;';
										$loc_str .= $obj->city.'|';
									}
									if($obj->state_initial !=''){
										$location_address .= $obj->state_initial.',&nbsp;';
										$loc_str .= $obj->state_initial.'|';
									}
									if($obj->zip !=''){
										$location_address .= $obj->zip;
										$loc_str .= $obj->zip;
									}
								?>
								<script>
								(function($) {
									$( document ).ready(function() {
										$(".video_<?php echo $i;?>").click(function() {
                                        jQuery.fancybox({
                                                    href : "<?php echo plugins_url('popup.php'); ?>?ID=<?php echo $mid;?>&loc_num=<?php echo $i;?>&logo=<?php echo $rows[0]->logo_img;?>&add=<?php echo $loc_str?>&tot=<?php echo $totrows;?>&mname=<?php echo $rows[0]->name;?>",
                                                    type : 'iframe',
                                                    width: 600,
                                                    padding : 5,
                                                    'onComplete' : function() {
                                                        jQuery.fancybox.showActivity();
                                                        jQuery('#fancybox-frame').load(function() { // wait for frame to load and then gets it's height
                                                        jQuery.fancybox.hideActivity();
                                                        jQuery('#fancybox-content').height($(this).contents().find('body').height()+30);
                                                        });
                                                      }
                                                });
										});
									 });
								}(jQuery));
                                </script>
									<tr>
										<td>
                                         <div style="float:left; padding-right:35px;"><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/coupon-app-v2/assets/business-logo/<?php echo $rows[0]->logo_img;?>" alt="" style="height:100px;"></div>
											<p class="loc_nm">Location <?php echo $i; ?> - <span style="color:#F96;"><?php echo $totrows >1?$totrows.' Coupons':$totrows.' Coupon';?></span></p>
											<p class="loc_con"><?php echo $location_address; ?>
										</td>
					
										<!--<td>View Coupon</td>-->
					
										<td class="alignright"><!--<a class="btn_orn" href="<?php //echo get_permalink(11762);?>?mid=<?php echo base64_encode($mid); ?>">View Coupons</a>-->
                                        <a class="video_<?php echo $i;?> btn_orn" href="javascript:void(0);">View Coupons</a>
                                        </td>
									</tr>
						
								 <?php
								 	$i++;
								}
						
								?>
                                </tbody>
                			</table>
            			</div>
    				</div>
        		</section>
    <?php
	}
}
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php twentythirteen_post_nav(); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>