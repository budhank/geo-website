<?php
include('../../wp-load.php');
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php wp_head(); ?>

<style type="text/css">
a {
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
	color: #04a45c;
	outline: none;
	text-decoration: none;
}
a:hover, a:focus {
	color: #000;
	outline:none;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	transition: all 0.2s linear;
	text-decoration: none;
}
.right {
	float: right;
}
.alignleft {
	text-align: left;
}
.alignright {
	text-align: right;
}
.popup_wrap{
	padding:20px;
	background:#fff;
	border-radius:5px;
}
.popup_sec{
	background:#fff;
	padding-top:20px;
}
.popup_sec h2{
	margin-bottom:0;
}
.orng_col{
	color:#33cc33;
}
.popup h2{
	font-size:24px;
	line-height:26px;
	text-transform:capitalize;
	font-weight:bold;
	color:#000;
	padding-bottom:20px;
	margin:0;
}
.popup h2 span{
	display:inline-block;
	font-size:20px;
	line-height:22px;
	cursor:pointer;
	font-weight:normal;
}
.popup_wrap{
	padding:20px;
}
.popup_inner{
	border:2px dashed #d7d7d7;
	padding:20px 10px;
	margin-bottom:20px;
}
.li_red_col{
	color:#ff6735;
}
.popup_inner p.li_red_col{
	padding-bottom:10px;
}
.popup_inner p{
	margin:0;
}
.popup h3{
	color:#000;
	font-size:22px;
	line-height:24px;
	text-transform:capitalize;
	font-weight:normal;
	padding-bottom:20px;
	margin:0;
}
.location_list_sec h3{
	color:#000;
	font-size:22px;
	line-height:24px;
	text-transform:capitalize;
	font-weight:normal;
	padding-bottom:10px;
	margin:0 0 10px;
	border-bottom:1px solid #d7d7d7;
}
.btn_orn{
	display:inline-block;
	font-size:18px;
	line-height:20px;
	padding:10px 20px;
	color:#fff;
	background:#33cc33;
	border:2px solid #33cc33;
	border-radius:4px;
}
.btn_orn:hover{
	border-color:#000;
	color:#000;
}
.ass_col{
	color:#bfbfbf;
}
.ass_col:hover{
	color:#000;
}
.table tr{
	border-bottom:1px solid #bfbfbf;
}
.table tr td{
	padding:10px;
	border:none !important;
	vertical-align:middle !important;
}
.table tr p{
	padding:5px 0;
}
.table{
	font-size:16px;
}
.banner{
	width:100%;
	background:#012229;
}
.banner h2{
	padding-bottom:50px;
	color:#fff;
	font-size:28px;
	line-height:30px;
}
.coupons_ban_img{
	display:inline-block;
	vertical-align:top;
	width:29.5%;
}
.coupons_ban_img img{
	width:100%;
}
.coupons_srch{
	display:inline-block;
	vertical-align:top;
	width:69.5%;
	padding-left:50px;
}
.coupons_srch .form-control{
	height:50px;
	line-height:50px;
	width:100%;
	border:none;
	border-bottom:2px solid #fff;
	color:#999999;
	font-size:16px;
	box-shadow:none;
	outline:none;
	background:#012229;
	-webkit-transition: all 0.2s ease;/* Safari 3.2+, Chrome */
	-moz-transition: all 0.2s ease;/* Firefox 4-15 */
	-o-transition: all 0.2s ease;/* Opera 10.5-12.00 */
	transition: all 0.2s ease;/* Firefox 16+, Opera 12.50+ */
	background:#fff;
}
.coupons_srch .form-control:focus{
	background:#02343f;
	color:#fff;
	-webkit-transition: all 0.2s ease;/* Safari 3.2+, Chrome */
	-moz-transition: all 0.2s ease;/* Firefox 4-15 */
	-o-transition: all 0.2s ease;/* Opera 10.5-12.00 */
	transition: all 0.2s ease;/* Firefox 16+, Opera 12.50+ */
	border-radius:0;
}
.coupons_srch_inner .btn_orn{
	height:50px;
	line-height:46px;
	padding:0 20px;
	vertical-align:middle;
	margin-left:15px;
}
.coupons_srch_inner .btn_orn:hover{
	border-color:#33cc33 ;
}
.coupons_srch{
	padding-top:30px;
}
.form-group{
	width:50%;
}
.loc_nm {
    color: #000;
    font-size: 22px;
    margin: 0;
    padding-bottom: 20px;
    text-transform: capitalize;
}
.loc_con {
    color: #999999;
    font-size: 20px;
    margin: 0;
    padding-bottom: 0;
    text-transform: capitalize;
}
.title2 {
    /*border-bottom: 1px solid #d7d7d7;*/
    color: #000;
    font-size: 22px !important;
    font-weight: normal;
    line-height: 2px !important;
    /*margin: 0 0 36px;*/
    /*padding-bottom: 10px;*/
    /*text-transform: capitalize;*/
}
@media screen and (max-width: 767px){
	.coupons_ban_img{
		display:none;
	}
	.coupons_srch{
		width:100%;
		padding:10px;
	}
	.coupons_srch_inner .form-group{
		display:inline-block;
		vertical-align:middle;
		margin:0;
	}
	.coupons_srch{
		text-align:center;
	}
	.banner h2{
		padding-bottom:20px;
	}
}



</style>
<?php
$vparam 	= $_REQUEST['ID'];
$loc_num 	= $_REQUEST['loc_num']?$_REQUEST['loc_num']:'';
$logo		= $_REQUEST['logo']?$_REQUEST['logo']:'';
$add		= $_REQUEST['add']?$_REQUEST['add']:'';
$tot		= $_REQUEST['tot']?$_REQUEST['tot']:'';
$mname		= $_REQUEST['mname']?stripslashes($_REQUEST['mname']):'';



$location_add = str_replace('|',',&nbsp;',$add);
$mydb = new wpdb('geocoupo_john','[=l07.G~i{#J','geocoupo_system_v2','localhost');
$rows = $mydb->get_results("SELECT mbc.* FROM mob_business_coupon AS mbc WHERE mbc.buss_id=".$vparam." ORDER BY mbc.name ASC"); 
//echo $rows[0]->total_coupon;
//$totrowsres = $mydb->get_results("SELECT count(*) as total_coupon FROM mob_business_coupon WHERE buss_id=".$mid."");
//$totrows = $totrowsres[0]->total_coupon > 0?$totrowsres[0]->total_coupon:'';
?>
<html>
<body <?php body_class(); ?>>
<div class="popup_sec">
    <?php
	/*$rows2 = $mydb->get_results("SELECT 
									mob_mst_business.bcat_id,
									mob_mst_business.name,
									mob_business_location.address1,
									mob_business_location.address2,
									mob_business_location.city,
									mob_business_location.state_initial,
									mob_mst_business.logo_img,
									mob_business_location.zip 
									FROM mob_mst_business 
									INNER JOIN `mob_business_location` ON `mob_mst_business`.`buss_id` = `mob_business_location`.`buss_id`
									INNER JOIN `mob_mst_bcategory` ON `mob_mst_bcategory`.`cat_id` = `mob_mst_business`.`bcat_id` 
									WHERE `mob_mst_business`.`buss_id`=".$vparam); */
	//echo $rows2[0]->logo_img;
	/*echo count($rows2);
	echo "<pre>";
	print_r($rows2);
	echo "</pre>";*/
	$totalrec = count($rows2);
	?>
    <div style="width:100%; overflow:hidden;">
    	<div style="float:left;"><img class="" src="http://<?php echo $_SERVER['SERVER_NAME']; ?>/coupon-app-v2/assets/business-logo/<?php echo $logo;?>" alt="" style="height:100px;"></div>
        <div style="float:left; margin-left:15px;">
        	<p class="title2"><?php echo $mname;?></p>
            <p style="margin:0 0 0px; font-weight:bold"><span style="color:#000;">Location &nbsp;<?php echo $loc_num;?></span>-<span style="color:#F96;"><?php echo $tot >1?$tot.' Coupons':$tot.' Coupon';?></span></p>
            <p style="margin:0 0 10px;"><?php echo $location_add;?></p>
        </div>
    </div>    
	<h2 class="aligncenter">Coupons</h2>
	<div class="popup_wrap">
    <?php
	if(count($rows)>0){
		foreach($rows as $obj){
		?>
	  	<div class="popup_inner">
			<h3><?php echo $obj->name;?></h3>
			<p><?php echo $obj->details;?></p>
            <p class="li_red_col">Expires: One year after activating the app.</p>
            <p><?php echo $obj->disclaimer;?></p>
		</div>
    	<?php 
	   } 
	}
	?>    
	</div>
</div>
</body>
</html>

